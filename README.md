# governance

A repository on how we run the company, what software we use, and what projects we need to focus on. 



# Why? 

We need a place to come back to, and also our members to read something when they want to. Culture is communication. Content is part of communication. 


We want to be independent of power plays regarding our internal communication and processes. In this way, we create resiliance. We learned this lesson from our [landing page](www.sail.black). We applied mainly independent software of our own making, but relied on cloud services for our servers. In the end, not our software was vulnerable, but the cloud service. Still, we secured the [landing page](www.sail.black) to such an extent that the landing page was still working after at least two billion-dollar companies attacked it. Our approach proves to make us very resilient and competitive with the largest players in the industry. 

# Short list of projects

We need some software we can extend quickly to run our business properly and lean. Still, we need to control most of our internal processes.


* reproducible rapid modelling 
* secure programming 
* Rapid API deployments 
* Business backends 

# Funding 

`We do not take any funding for software development`. Not even donations! Period. Why? Because we suffered
and continue to suffer from commercialized software. comemrcial software is almost always bs and 
the products are more of a torture than they are helpful. 

For example, for our algorithms we face serious competetion from FANG and other large companies. 
So far so good. But there are cyber holes everywhere and some adversaries don't think in their 
profit and how they can help their customers but rather in how others do it, and how they can 
stop others from doing it. 

We do not want to become such a player, nor do we want that our software becomes obsolete, becuase
it relies on funding.

So we only take funding for other business activites, like production lines, sales, offices, etc.

# Security 

We invented novel packages and workflows to ensure more secure development and security practices 
throughout the last year. Those are mostly open and everyone can use them. Why? Because we will 
be more secure, too. As the activity had the goal of becoming more secure anyways, it is the right
decisions. Again, competitors are trying to make money from it, but in the end the companies will 
fail for the same reasons software with funding tends to fail.

Seriously look at my favorite company. Throughout my life I am forced to use MS products. Like 
seriously forced. At work, I had to be online at teams, had to use MS Office instead of better 
options, had to pay for a subscription to MS Office when setting up the domain at the domain host
etc... Can such a business become a good business? In our opinion: NO!

# Resistance 

Okay, you could say ***** your competitors once, they will stop. But, no they won't. It is why we 
do not do an eye for an eye in our wider culture in Europe, too. We only do `non-violent resistance`. 
That means for us: sticking to our values, vision, and foremost never stop developing our products.
And also simply, to educate people. 

# Hirarchy

Do what you want! I am not here to impose any opinion, business model, product, ego, doubt, world view, 
religion, preference, taste, style or whatever on anyone. I code because I like it and I need it 
for my business. Thus, most actively developed packages under my name and the organiziation are
creating real world value becuase they are necessary. If you want to use them for something else, then
please do it! 

If you want to work with us, send me a mail, contact me on twitter, buy me a coffee, play a round 
of tennis with me, book a service, buy a shirt, call me, whatever it is. If not, fine, then just
stay away and mind your business, lad. 
